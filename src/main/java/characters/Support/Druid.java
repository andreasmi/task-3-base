package main.java.characters.Support;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.Healer;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Leather;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.spells.abstractions.DamagingSpell;
import main.java.spells.abstractions.HealingSpell;

/**
 Druids are spell casters who use nature based magic to aid their allies in battle.
 They can heal their allies or protect them using the forces of nature.
 As a support class they only have defensive stats.
*/
public class Druid extends Character implements Healer{

    //Spell
    private HealingSpell healingSpell;


    public Druid() {
        super(CharacterBaseStatsDefensive.DRUID_BASE_HEALTH,
                CharacterBaseStatsDefensive.DRUID_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.DRUID_BASE_MAGIC_RES,
                ArmorType.Leather, MagicWeapon.class, Leather.class);
    }


    /**
     *  Setiing a new healing spell
     * @param healingSpell The healing spell to equip
     */
    public void setHealingSpell(HealingSpell healingSpell){
        this.healingSpell = healingSpell;
    }

    /**
     * Heals a party member
     * @return The amount of health to heal a character
     */
    @Override
    public double healPartyMember() {
        try{
            return healingSpell.getHealingAmount() * ((MagicWeapon) equippedWeapon).getMagicPowerModifier() * ((MagicWeapon) equippedWeapon).getRarity().getPowerModifier();
        }catch (NullPointerException ex){
            //Let user know the Healer dosn't have any healing spell or weapon equipped
            return 0;
        }
    }



}
