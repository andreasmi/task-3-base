package main.java.characters.Support;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.Shielder;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Cloth;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.spells.abstractions.ShieldingSpell;

/**
 Priest are the servants of the light and goodness.
 They use holy magic to heal and shield allies.
 As a support class they only have defensive stats.
*/
public class Priest extends Character implements Shielder {

    //Spell
    private ShieldingSpell shieldingSpell;

    public Priest() {
        super(CharacterBaseStatsDefensive.PRIEST_BASE_HEALTH,
                CharacterBaseStatsDefensive.PRIEST_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.PRIEST_BASE_MAGIC_RES,
                ArmorType.Cloth, MagicWeapon.class, Cloth.class);
    }

    /**
     * Setting a new shielding spell
     * @param shieldingSpell The spell to equip
     */
    public void setShieldingSpell(ShieldingSpell shieldingSpell){
        this.shieldingSpell = shieldingSpell;
    }


    /**
     * Shields a party member for a percentage of their maximum health.
     * @param partyMemberMaxHealth The max health of the part memeber to shield
     */
    @Override
    public double shieldPartyMember(double partyMemberMaxHealth) {
        try{
            return (partyMemberMaxHealth * shieldingSpell.getAbsorbShieldPercentage()) +
                    (((MagicWeapon) equippedWeapon).getMagicPowerModifier() * ((MagicWeapon) equippedWeapon).getRarity().getPowerModifier());
        }catch (NullPointerException ex){
            //Trigger message to console: "This Priest dosn't have any weapon or spell equipped"
            return 0; // Return calculated shield value
        }
    }

}
