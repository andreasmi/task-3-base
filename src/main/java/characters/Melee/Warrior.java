package main.java.characters.Melee;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.BladedMelee;
import main.java.characters.abstractions.Character;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Plate;
import main.java.items.weapons.abstractions.BladedWeapon;

/**
 Warriors are combat veterans, durable forces on the battlefield.
 They are masters of the blade and wield it with unmatched ferocity.
*/
public class Warrior extends Character implements BladedMelee {

    // Base stats offensive
    private double baseAttackPower;

    public Warrior() {
        super(CharacterBaseStatsDefensive.WARRIOR_BASE_HEALTH,
                CharacterBaseStatsDefensive.WARRIOR_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.WARRIOR_BASE_MAGIC_RES,
                ArmorType.Plate, BladedWeapon.class, Plate.class);
        this.baseAttackPower = CharacterBaseStatsOffensive.WARRIOR_MELEE_ATTACK_POWER;
    }

    /**
     * Damages the enemy
     * @return The amount of damage the character sends
     */
    @Override
    public double attackWithBladedWeapon() {
        try{
            return baseAttackPower *
                    ((BladedWeapon) equippedWeapon).getAttackPowerModifier() *
                    ((BladedWeapon) equippedWeapon).getRarity().getPowerModifier();
        }catch (NullPointerException ex){
            //Let the user know they dont have any weapon equipped
            return 0;
        }

    }

}
