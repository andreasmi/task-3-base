package main.java.characters.Melee;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.BladedMelee;
import main.java.characters.abstractions.Character;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Leather;
import main.java.items.weapons.abstractions.BladedWeapon;
import main.java.items.weapons.abstractions.BluntWeapon;

/**
 Rogues are stealthy combatants of the shadows.
 They wield bladed weapons with great agility and dispatch their enemies swiftly.
*/
public class Rogue extends Character implements BladedMelee {

    // Base stats offensive
    private double baseAttackPower;

    public Rogue() {
        super(CharacterBaseStatsDefensive.ROGUE_BASE_HEALTH,
                CharacterBaseStatsDefensive.ROGUE_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.ROGUE_BASE_MAGIC_RES,
                ArmorType.Leather, BladedWeapon.class, Leather.class);
        this.baseAttackPower = CharacterBaseStatsOffensive.ROGUE_MELEE_ATTACK_POWER;
    }

    /**
     * Damages the enemy
     * @return The amount of damage the character sends
     */
    @Override
    public double attackWithBladedWeapon() {
        try{
            return baseAttackPower *
                    ((BladedWeapon) equippedWeapon).getAttackPowerModifier() *
                    ((BladedWeapon) equippedWeapon).getRarity().getPowerModifier();
        }catch (NullPointerException ex){
            //Let the user know they dont have any weapon equipped
            return 0;
        }
    }

}
