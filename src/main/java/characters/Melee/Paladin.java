package main.java.characters.Melee;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.BluntMelee;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.Shielder;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Plate;
import main.java.items.weapons.abstractions.BladedWeapon;
import main.java.items.weapons.abstractions.BluntWeapon;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.spells.abstractions.ShieldingSpell;

/**
 Paladins are faithful servants of the light and everything holy.
 They dispatch justice and are filled with vengeance for the wrongs done to society by evil.
 Paladins are very durable and typically wield heavy weapons.
*/
public class Paladin extends Character implements BluntMelee {

    // Base stats offensive
    private double baseAttackPower;

    public Paladin() {
        super(CharacterBaseStatsDefensive.PALADIN_BASE_HEALTH,
                CharacterBaseStatsDefensive.PALADIN_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.PALADIN_BASE_MAGIC_RES,
                ArmorType.Plate, BluntWeapon.class, Plate.class);
        this.baseAttackPower = CharacterBaseStatsOffensive.PALADIN_MELEE_ATTACK_POWER;
    }


    /**
     * Damages the enemy
     * @return The amount of damage the character sends
     */
    @Override
    public double attackWithBluntWeapon() {
        try{
            return baseAttackPower *
                    ((BluntWeapon) equippedWeapon).getAttackPowerModifier() *
                    ((BluntWeapon) equippedWeapon).getRarity().getPowerModifier();
        }catch (NullPointerException ex){
            //Let the user know they dont have any weapon equipped
            return 0;
        }

    }
}
