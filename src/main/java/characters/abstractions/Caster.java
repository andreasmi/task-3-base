package main.java.characters.abstractions;

/**
 * Interface for Caster
 */
public interface Caster {
    double castDamagingSpell();
}
