package main.java.characters.abstractions;

/**
 * Interface for Shielder
 */
public interface Shielder {
    double shieldPartyMember(double partyMemberMaxHealth);
}
