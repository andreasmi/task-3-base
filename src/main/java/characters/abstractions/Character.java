package main.java.characters.abstractions;

import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;

/**
 * The Character class is an abstract class that implements the iCharacter interface.
 * This class will be extended by every character.
 */
public abstract class Character implements iCharacter {

    // Base stats defensive
    private final double baseHealth;
    private double basePhysReductionPercent; // Armor
    private double baseMagicReductionPercent; // Magic armor

    //Armor and weapon types
    public ArmorType ARMOR_TYPE;
    public Class ALLOWED_WEAPON;
    public Class ALLOWED_ARMOR;

    // Active trackers and flags
    private double currentHealth;
    private Boolean isDead = false;
    protected Weapon equippedWeapon;
    protected Armor equippedArmor;

    /**
     *
     * @param baseHealth The characters base and max health
     * @param basePhysReductionPercent The characters basic physical reduction percentage
     * @param baseMagicReductionPercent The characters basic magic reduction percentage
     * @param armorType The characters armor type
     * @param allowedWeapon The characters allowed weapon class
     * @param allowedArmor The characters allowed armor class
     */
    public Character(double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent, ArmorType armorType, Class allowedWeapon, Class allowedArmor){
        this.baseHealth = baseHealth;
        this.basePhysReductionPercent = basePhysReductionPercent;
        this.baseMagicReductionPercent = baseMagicReductionPercent;
        this.ARMOR_TYPE = armorType;
        this.ALLOWED_WEAPON = allowedWeapon;
        this.ALLOWED_ARMOR = allowedArmor;
        this.currentHealth = this.baseHealth;
    }

    /**
     * This function is to get the current health of the characters
     * @return The Current health of the character
     */
    @Override
    public double getCurrentHealth() {
        return currentHealth;
    }

    /**
     * These function is to get the max health of the character
     * @return The max health of the character
     */
    @Override
    public double getCurrentMaxHealth() {
        try{
            return baseHealth * equippedArmor.getHealthModifier() * equippedArmor.getRarityModifier();
        }catch (NullPointerException ex){
            return 0;
        }
    }

    /**
     *
     * @return If the character is dead or not
     */
    @Override
    public Boolean getDead() {
        return isDead;
    }

    /**
     *
     * @return The type of weapon the character can have
     */
    public WeaponType getWeapon(){
        if(equippedWeapon != null){
            return equippedWeapon.getWeaponType();
        }
        return null;
    }

    /**
     *
     * @return The type of armor the character can have
     */
    public ArmorType getArmor(){
        if(equippedArmor != null){
            return equippedArmor.getArmorType();
        }
        return null;
    }

    /**
     * This function is used to both heal and attack a character
     * @param changeAmount The amount of health that the characters health is going to be change
     */
    @Override
    public void modifyHealth(double changeAmount){
        //Can't heal the character if it's already dead
        if(!isDead){
            currentHealth += changeAmount;
            //Sets the isDead flag if the character has less than or equals to 0 health
            if(currentHealth <= 0){
                isDead = true;
            }
        }
    }

    /**
     * Equips armor to the character, modifying stats.
     * @param armor Which armor to equip
     */
    @Override
    public Boolean equipArmor(Armor armor) {
        try{
            if(ALLOWED_ARMOR.isAssignableFrom(armor.getClass())){
                equippedArmor = armor;
                return true;
            }
            return false;
        }catch (NullPointerException ex){
            return false;
        }

    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon Which weapon to equip
     */
    @Override
    public Boolean equipWeapon(Weapon weapon) {
        try{
            if(ALLOWED_WEAPON.isAssignableFrom(weapon.getClass())){
                equippedWeapon = weapon;
                return true;
            }
            return false;
        }catch (NullPointerException ex){
            return false;
        }

    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage The amount of damage that is sent to the character
     * @param damageType The damage type that is being sent to the character
     */
    @Override
    public double takeDamage(double incomingDamage, DamagaType damageType) {
        //Switch on damage type and if the equation returns less than 1, the returns 1
        try{
            double damage;
            switch (damageType){
                case Attack:
                    damage = incomingDamage * (1-(basePhysReductionPercent * equippedArmor.getPhysRedModifier() * equippedArmor.getRarityModifier()));
                    if(damage < 1){
                        return 1;
                    }
                    return damage;
                case Magic:
                    damage = incomingDamage * (1-(baseMagicReductionPercent * equippedArmor.getMagicRedModifier() * equippedArmor.getRarityModifier()));
                    if(damage < 1){
                        return 1;
                    }
                    return damage;
                default:
                    return 0;
            }
        } catch (NullPointerException ex){
            return 0;
        }

    }
}
