package main.java.characters.abstractions;

/**
 * A enum of the different types of characters that exists
 */
public enum CharacterType {
    Druid,
    Mage,
    Paladin,
    Priest,
    Ranger,
    Rogue,
    Warlock,
    Warrior
}
