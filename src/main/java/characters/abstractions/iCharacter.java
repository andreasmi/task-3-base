package main.java.characters.abstractions;

import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.Weapon;

/**
 * Interface for the Character. Every character will extend the class that implements these interface
 */
public interface iCharacter {
    double getCurrentHealth();
    double getCurrentMaxHealth();
    Boolean getDead();
    Boolean equipArmor(Armor armor);
    Boolean equipWeapon(Weapon weapon);
    double takeDamage(double incomingDamage, DamagaType damagaType);
    void modifyHealth(double changeAmount);
}
