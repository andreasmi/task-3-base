package main.java.characters.abstractions;

/**
 * Interface for Ranged
 */
public interface Ranged {
    double attackWithRangedWeapon();
}
