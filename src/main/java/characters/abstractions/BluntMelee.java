package main.java.characters.abstractions;

/**
 * Interface for BlauntMelee
 */
public interface BluntMelee {
    double attackWithBluntWeapon();
}
