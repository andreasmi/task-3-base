package main.java.characters.abstractions;

/**
 * Interface for Healer
 */
public interface Healer{
    double healPartyMember();
}
