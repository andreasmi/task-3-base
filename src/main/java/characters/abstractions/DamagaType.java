package main.java.characters.abstractions;

/**
 * The different types of damage a character can take
 */
public enum  DamagaType {
    Attack,
    Magic
}
