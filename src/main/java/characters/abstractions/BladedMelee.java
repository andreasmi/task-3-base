package main.java.characters.abstractions;

/**
 * Interface for BladedMelee
 */
public interface BladedMelee{
    double attackWithBladedWeapon();
}
