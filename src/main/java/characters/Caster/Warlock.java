package main.java.characters.Caster;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Caster;
import main.java.characters.abstractions.Character;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Cloth;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.spells.abstractions.DamagingSpell;

/**
 Warlocks are masters of chaos, entropy, and death. They were once mages but were corrupted by power.
 They can conjure up pure chaos energy to destroy their enemies.
*/
public class Warlock extends Character implements Caster {

    // Metadata
    private DamagingSpell damagingSpell;
    // Base stats offensive
    private double baseMagicPower;

    public Warlock() {
        super(CharacterBaseStatsDefensive.WARLOCK_BASE_HEALTH,
                CharacterBaseStatsDefensive.WARLOCK_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.WARLOCK_BASE_MAGIC_RES,
                ArmorType.Cloth, MagicWeapon.class, Cloth.class);
        this.baseMagicPower = CharacterBaseStatsOffensive.WARLOCK_MAGIC_POWER;
    }

    /**
     * This function is to set a new damaging spell
     * @param damagingSpell The spell to equip
     */
    public void setDamagingSpell(DamagingSpell damagingSpell){
        this.damagingSpell = damagingSpell;
    }


    /**
     * Damages the enemy with its spells
     * @return The amount of damage the spell sends
     */
    @Override
    public double castDamagingSpell() {
        try{
            return baseMagicPower * ((MagicWeapon) equippedWeapon).getMagicPowerModifier() *
                    ((MagicWeapon) equippedWeapon).getRarity().getPowerModifier();
        }catch (NullPointerException ex){
            //Let the user know they dont have any weapon equipped
            return 0;
        }
    }

}
