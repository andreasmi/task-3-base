package main.java.characters.Caster;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Caster;
import main.java.characters.abstractions.Character;
import main.java.items.armor.Cloth;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.spells.abstractions.DamagingSpell;

/**
 Mages are masters of arcane magic. Their skills are honed after years of dedicated study.
 They conjure arcane energy to deal large amounts of damage to enemies.
 They are vulnerable to physical attacks but are resistant to magic.
*/
public class Mage extends Character implements Caster {
    // Metadata
    private DamagingSpell damagingSpell;
    // Base stats offensive
    private final double baseMagicPower;

    // Constructor
    public Mage() {
        super(CharacterBaseStatsDefensive.MAGE_BASE_HEALTH,
                CharacterBaseStatsDefensive.MAGE_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.MAGE_BASE_MAGIC_RES,
                ArmorType.Cloth, MagicWeapon.class, Cloth.class);
        this.baseMagicPower = CharacterBaseStatsOffensive.MAGE_MAGIC_POWER;
    }

    /**
     * This function is to set a new damaging spell
     * @param damagingSpell The spell to equip
     */
    public void setDamagingSpell(DamagingSpell damagingSpell){
        this.damagingSpell = damagingSpell;
    }

    /**
     * Damages the enemy with its spells
     * @return The amount of damage that the spell does
     */
    @Override
    public double castDamagingSpell() {
        try{
            return (baseMagicPower * ((MagicWeapon) equippedWeapon).getMagicPowerModifier() *
                    ((MagicWeapon) equippedWeapon).getRarity().getPowerModifier());
        }catch (NullPointerException ex){
            //Let the user know they dont have any equipped weapon
            return 0;
        }
    }


}
