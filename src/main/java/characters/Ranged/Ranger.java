package main.java.characters.Ranged;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.Ranged;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Mail;
import main.java.items.weapons.abstractions.RangedWeapon;

/**
 Rangers are masters of ranged combat. They use a wide arsenal of weapons to dispatch enemies.
*/
public class Ranger extends Character implements Ranged {

    // Base stats offensive
    private double baseAttackPower;

    public Ranger() {
        super(CharacterBaseStatsDefensive.RANGER_BASE_HEALTH,
                CharacterBaseStatsDefensive.RANGER_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.RANGER_BASE_MAGIC_RES,
                ArmorType.Mail, RangedWeapon.class, Mail.class);
        this.baseAttackPower = CharacterBaseStatsOffensive.RANGER_RANGED_ATTACK_POWER;
    }

    /**
     * Damages the enemy
     * @return The amount of damage the character sends
     */
    @Override
    public double attackWithRangedWeapon() {
        try{
            return baseAttackPower * ((RangedWeapon) equippedWeapon).getAttackPowerModifier() * ((RangedWeapon) equippedWeapon).getRarity().getPowerModifier(); // Replaced with actual damage amount based on calculations
        }catch (NullPointerException ex){
            //Let the user know they don't have a weapon equipped
            return 0;
        }
    }


}
