package main.java.demonstrationHelpers;

import main.java.characters.Caster.Mage;
import main.java.characters.Caster.Warlock;
import main.java.characters.Melee.Warrior;
import main.java.characters.Ranged.Ranger;
import main.java.characters.Support.Druid;
import main.java.characters.Support.Priest;
import main.java.characters.abstractions.*;
import main.java.characters.abstractions.Character;
import main.java.consolehelpers.Color;
import main.java.factories.*;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.*;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.BladedWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.spells.abstractions.*;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


//This class is made to demonstrate a few of the functionalities of the game


public class GameDemonstration {
    ArrayList<Weapon> randomWeapons;
    ArrayList<Armor> randomArmor;
    ArrayList<Spell> randomSpells;
    ArrayList<Character> randomCharacters;
    public void GameDemonstration(){

        //Creating some characters
        Character mage = CharacterFactory.getCharacter(CharacterType.Mage);
        Character warlock = CharacterFactory.getCharacter(CharacterType.Warlock);
        Character paladin = CharacterFactory.getCharacter(CharacterType.Paladin);
        Character rogue = CharacterFactory.getCharacter(CharacterType.Rogue);
        Character warrior = CharacterFactory.getCharacter(CharacterType.Warrior);
        Character ranger = CharacterFactory.getCharacter(CharacterType.Ranger);
        Character druid = CharacterFactory.getCharacter(CharacterType.Druid);
        Character priest = CharacterFactory.getCharacter(CharacterType.Priest);

        //Creating some weapons with different kinds of rarity
        Weapon staff = WeaponFactory.getItem(WeaponType.Staff, new Common());
        Weapon wand = WeaponFactory.getItem(WeaponType.Wand, new Epic());
        Weapon axe = WeaponFactory.getItem(WeaponType.Axe, new Uncommon());
        Weapon dagger = WeaponFactory.getItem(WeaponType.Dagger, new Legendary());
        Weapon hammer = WeaponFactory.getItem(WeaponType.Hammer, new Rare());
        Weapon mace = WeaponFactory.getItem(WeaponType.Mace, new Uncommon());
        Weapon sword = WeaponFactory.getItem(WeaponType.Sword, new Legendary());
        Weapon bow = WeaponFactory.getItem(WeaponType.Bow, new Epic());
        Weapon crossbow = WeaponFactory.getItem(WeaponType.Crossbow, new Legendary());
        Weapon gun = WeaponFactory.getItem(WeaponType.Gun, new Common());



        //Creating some armor with different rarity
        Armor cloth = ArmorFactory.getArmor(ArmorType.Cloth, new Common());
        Armor leather = ArmorFactory.getArmor(ArmorType.Leather, new Epic());
        Armor mail = ArmorFactory.getArmor(ArmorType.Mail, new Legendary());
        Armor plate = ArmorFactory.getArmor(ArmorType.Plate, new Uncommon());

        //Creating some spells
        Spell arcaneMissile = SpellFactory.getSpell(SpellType.ArcaneMissile);
        Spell chaosBolt = SpellFactory.getSpell(SpellType.ChaosBolt);
        Spell regrowth = SpellFactory.getSpell(SpellType.Regrowth);
        Spell swiftMend = SpellFactory.getSpell(SpellType.Swiftmend);
        Spell barrier = SpellFactory.getSpell(SpellType.Barrier);
        Spell rapture = SpellFactory.getSpell(SpellType.Rapture);


        //Characters equipping weapon, armor and spells
        //Mage
        mage.equipWeapon(staff);
        mage.equipArmor(cloth);
        ((Mage) mage).setDamagingSpell((DamagingSpell) arcaneMissile);

        //Warlock
        warlock.equipWeapon(wand);
        warlock.equipArmor(cloth);
        ((Warlock) warlock).setDamagingSpell((DamagingSpell) chaosBolt);

        //Paladin
        paladin.equipWeapon(hammer);
        paladin.equipArmor(plate);

        //Rogue
        rogue.equipWeapon(axe);
        rogue.equipArmor(leather);

        //Warrior
        warrior.equipWeapon(dagger);
        warrior.equipArmor(plate);

        //Ranger
        ranger.equipWeapon(crossbow);
        ranger.equipArmor(mail);

        //Druid
        druid.equipWeapon(wand);
        druid.equipArmor(leather);
        ((Druid) druid).setHealingSpell((HealingSpell) regrowth);

        //Priest
        priest.equipWeapon(staff);
        priest.equipArmor(cloth);
        ((Priest) priest).setShieldingSpell((ShieldingSpell) barrier);


        warlock.modifyHealth(warlock.takeDamage(((Mage) mage).castDamagingSpell(), DamagaType.Magic)*-1);
        System.out.println(Color.BLUE+"Warlock was attacked by Mage, warlock health: " + warlock.getCurrentHealth()+Color.RESET);

        warlock.modifyHealth(warlock.takeDamage(((Mage) mage).castDamagingSpell(), DamagaType.Magic)*-1);
        System.out.println(Color.BLUE+"Warlock was attacked by Mage, warlock health: " + warlock.getCurrentHealth()+Color.RESET);

        warlock.modifyHealth(warlock.takeDamage(((Mage) mage).castDamagingSpell(), DamagaType.Magic)*-1);
        System.out.println(Color.BLUE+"Warlock was attacked by Mage, warlock health: " + warlock.getCurrentHealth()+Color.RESET);

        paladin.modifyHealth(paladin.takeDamage(((Warrior) warrior).attackWithBladedWeapon(), DamagaType.Attack)*-1);
        System.out.println(Color.BLUE+"Paladin was attacked by Warrior, Paladin health: " + paladin.getCurrentHealth()+Color.RESET);

        paladin.modifyHealth(paladin.takeDamage(((Ranger) ranger).attackWithRangedWeapon(), DamagaType.Attack)*-1);
        System.out.println(Color.BLUE+"Paladin was attacked by Ranger, Paladin health: " + paladin.getCurrentHealth()+Color.RESET);

        druid.modifyHealth(druid.takeDamage(((Warlock) warlock).castDamagingSpell(), DamagaType.Magic)*-1);
        System.out.println(Color.YELLOW+"Druid was attacked by Warlock, Druid health: " + druid.getCurrentHealth()+Color.RESET);


        System.out.println("\n\n");
        System.out.println("Warlock is casting spell and does: " + ((Warlock) warlock).castDamagingSpell() + " damage");

        System.out.println("Druid is casting spell and heales: " + ((Druid) druid).healPartyMember());

        double warriorAttack = ((Warrior) warrior).attackWithBladedWeapon();
        System.out.println("Warrior attacks and does: " + warriorAttack + " damage");
        double mageTake = mage.takeDamage(warriorAttack, DamagaType.Attack);
        System.out.println("Mage takes " + mageTake + " damage from the warrior");


        /*//Lists of the different Items/Characters
        ArrayList<Character> characters = new ArrayList<>();
            characters.add(mage);
            characters.add(warlock);
            characters.add(paladin);
            characters.add(rogue);
            characters.add(warrior);
            characters.add(ranger);
            characters.add(druid);
            characters.add(priest);

        ArrayList<Weapon> weapons = new ArrayList<>();
        weapons.add(staff);
        weapons.add(wand);
        weapons.add(axe);
        weapons.add(dagger);
        weapons.add(hammer);
        weapons.add(mace);
        weapons.add(sword);
        weapons.add(bow);
        weapons.add(crossbow);
        weapons.add(gun);
        ArrayList<Armor> armor = new ArrayList<>();
        armor.add()
        ArrayList<Spell> spells = new ArrayList<>();

        //Creating some armor with different rarity
        Armor cloth = ArmorFactory.getArmor(ArmorType.Cloth, new Common());
        Armor leather = ArmorFactory.getArmor(ArmorType.Leather, new Epic());
        Armor mail = ArmorFactory.getArmor(ArmorType.Mail, new Legendary());
        Armor plate = ArmorFactory.getArmor(ArmorType.Plate, new Uncommon());

        //Creating some spells
        Spell arcaneMissile = SpellFactory.getSpell(SpellType.ArcaneMissile);
        Spell chaosBolt = SpellFactory.getSpell(SpellType.ChaosBolt);
        Spell regrowth = SpellFactory.getSpell(SpellType.Regrowth);
        Spell swiftMend = SpellFactory.getSpell(SpellType.Swiftmend);
        Spell barrier = SpellFactory.getSpell(SpellType.Barrier);
        Spell rapture = SpellFactory.getSpell(SpellType.Rapture);*/

        System.out.println("\n\n");
        randomWeapons = generateRandomWeapons(50);
        randomArmor = generateRandomArmor(50);
        randomSpells = generateRandomSpells(20);
        randomCharacters = generateRandomCharacters(10, randomArmor, randomWeapons, randomSpells);
        attackRandom(100);


    }

    private void attackRandom(int amountOfAttacks){
        Character currentAttacker;
        Character currentDefender;

        double damageSent = 0;
        double damageTaken = 0;

        Random rand = new Random();

        DecimalFormat decimalFormat = new DecimalFormat("#.00");

        for(int i = 0; i < amountOfAttacks; i++){
            currentAttacker = randomCharacters.get(rand.nextInt(randomCharacters.size()));
            currentDefender = randomCharacters.get(rand.nextInt(randomCharacters.size()));
            if(currentAttacker.getDead() || currentDefender.getDead()){
                continue;
            }
            if(currentAttacker instanceof BladedMelee){
                damageSent = Double.parseDouble(decimalFormat.format(((BladedMelee)currentAttacker).attackWithBladedWeapon()));
            }else if(currentAttacker instanceof BluntMelee){
                damageSent = Double.parseDouble(decimalFormat.format(((BluntMelee)currentAttacker).attackWithBluntWeapon()));
            }else if(currentAttacker instanceof Caster){
                damageSent = Double.parseDouble(decimalFormat.format(((Caster)currentAttacker).castDamagingSpell()));
            }else if(currentAttacker instanceof Healer){
                continue;
            }else if(currentAttacker instanceof Ranged){
                damageSent = Double.parseDouble(decimalFormat.format(((Ranged)currentAttacker).attackWithRangedWeapon()));
            }else if(currentAttacker instanceof Shielder){
                continue;
            }


            damageTaken = Double.parseDouble(decimalFormat.format(currentDefender.takeDamage(damageSent, DamagaType.Attack)));
            currentDefender.modifyHealth(damageTaken*-1);
            System.out.println(currentAttacker.getClass().getSimpleName() + " sent " + damageSent + " to " + currentDefender.getClass().getSimpleName() +
                    ", damage taken is " + damageTaken + ". " + currentDefender.getClass().getSimpleName() + " current health is " + currentDefender.getCurrentHealth());
            if(currentDefender.getDead()){
                System.out.println(Color.RED + currentDefender.getClass().getSimpleName() + " died" + Color.RESET);
            }
        }

    }

    private ArrayList<Weapon> generateRandomWeapons(int amountOfWeapons){
        ArrayList<Weapon> result = new ArrayList<>();
        Random rand = new Random();
        WeaponType type;
        for(int i = 0; i < amountOfWeapons; i++){
            type = WeaponType.values()[rand.nextInt(WeaponType.values().length)];
            result.add(WeaponFactory.getItem(type, generateRandomRarity()));
        }
        return result;
    }

    private ArrayList<Armor> generateRandomArmor(int amountOfArmor){
        ArrayList<Armor> result = new ArrayList<>();
        Random rand = new Random();
        ArmorType type;
        for(int i = 0; i < amountOfArmor; i++){
            type = ArmorType.values()[rand.nextInt(ArmorType.values().length)];
            result.add(ArmorFactory.getArmor(type, generateRandomRarity()));
        }
        return result;
    }

    private ArrayList<Character> generateRandomCharacters(int amountOfCharacters, ArrayList<Armor> armorList, ArrayList<Weapon> weaponList, ArrayList<Spell> spellList){
        ArrayList<Character> result = new ArrayList<>();
        Random rand = new Random();
        CharacterType charType;
        Character current;
        Boolean equipping = true;
        Boolean equippingSpell = true;
        Spell randomSpell;
        for(int i = 0; i < amountOfCharacters; i++){
            charType = CharacterType.values()[rand.nextInt(CharacterType.values().length)];
            current = CharacterFactory.getCharacter(charType);

            equipping = true;
            while(equipping){
                if(current.equipArmor(armorList.get(rand.nextInt(armorList.size())))){
                    equipping = false;
                }
            }

            equipping = true;
            while(equipping){
                if(current.equipWeapon(weaponList.get(rand.nextInt(weaponList.size())))){
                    equipping = false;
                }
            }

            if(current instanceof Mage){
                equippingSpell = true;
                while(equippingSpell){
                    randomSpell = spellList.get(rand.nextInt(spellList.size()));
                    if(randomSpell instanceof DamagingSpell){
                        ((Mage)current).setDamagingSpell((DamagingSpell) randomSpell);
                        equippingSpell = false;
                    }
                }
            }else if(current instanceof Druid){
                equippingSpell = true;
                while(equippingSpell){
                    randomSpell = spellList.get(rand.nextInt(spellList.size()));
                    if(randomSpell instanceof HealingSpell){
                        ((Druid)current).setHealingSpell((HealingSpell) randomSpell);
                        equippingSpell = false;
                    }
                }
            }else if(current instanceof Priest){
                equippingSpell = true;
                while(equippingSpell){
                    randomSpell = spellList.get(rand.nextInt(spellList.size()));
                    if(randomSpell instanceof ShieldingSpell){
                        ((Priest)current).setShieldingSpell((ShieldingSpell) randomSpell);
                        equippingSpell = false;
                    }
                }
            }else if(current instanceof Warlock){
                equippingSpell = true;
                while(equippingSpell){
                    randomSpell = spellList.get(rand.nextInt(spellList.size()));
                    if(randomSpell instanceof DamagingSpell){
                        ((Warlock)current).setDamagingSpell((DamagingSpell) randomSpell);
                        equippingSpell = false;
                    }
                }
            }

            result.add(current);
        }

        return result;
    }

    public ArrayList<Spell> generateRandomSpells(int amountOfSpells){
        ArrayList<Spell> result = new ArrayList<>();
        Random rand = new Random();
        SpellType type;
        for(int i = 0; i < amountOfSpells; i++){
            type = SpellType.values()[rand.nextInt(SpellType.values().length)];
            result.add(SpellFactory.getSpell(type));
        }
        return result;
    }

    private Rarity generateRandomRarity(){
        Random rand = new Random();
        return RarityFactory.getRarity(ItemRarity.values()[rand.nextInt(ItemRarity.values().length)]);
    }

}
