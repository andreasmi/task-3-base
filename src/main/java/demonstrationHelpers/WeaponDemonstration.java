package main.java.demonstrationHelpers;

import main.java.consolehelpers.Color;
import main.java.factories.WeaponFactory;
import main.java.items.rarity.Common;
import main.java.items.rarity.Legendary;
import main.java.items.rarity.Rare;
import main.java.items.rarity.Uncommon;
import main.java.items.weapons.abstractions.*;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.items.weapons.magic.Staff;
import main.java.items.weapons.magic.Wand;
import main.java.items.weapons.melee.Axe;
import main.java.items.weapons.melee.Hammer;
import org.w3c.dom.ranges.Range;

public class WeaponDemonstration {
    public void weaponDesmonstration(){
        MagicWeapon druidWeapon = (MagicWeapon) WeaponFactory.getItem(WeaponType.Staff, new Common());
        BluntWeapon paladinWeapon = (BluntWeapon) WeaponFactory.getItem(WeaponType.Hammer, new Legendary());
        //Due to polymorphism, the line under is not allowed since the ranger is only able to weild "Ranged" weapons
        //The weapon a ranger can initialize can't be something that doesn't implement the Ranged interface
        //RangedWeapon rangerWeapon = (RangedWeapon) new Axe(new Uncommon(), WeaponType.Axe);

        System.out.println(Color.CYAN+"Druid weapon:\n"+druidWeapon.getRarity().getItemRarityColor()+druidWeapon.getRarity().getRarityName()+" "+
                druidWeapon.getWeaponType()+ "| WeaponModifier: "+druidWeapon.getMagicPowerModifier() + " | WeaponRarityModifiwe: " +
                druidWeapon.getRarity().getPowerModifier());

        System.out.println(Color.CYAN+"Paladin weapon:\n"+paladinWeapon.getRarity().getItemRarityColor()+paladinWeapon.getRarity().getRarityName()+" "+
                paladinWeapon.getWeaponType()+ "| WeaponModifier: "+paladinWeapon.getAttackPowerModifier() + " | WeaponRarityModifiwe: " +
                paladinWeapon.getRarity().getPowerModifier());

        //Druid weapon can be reassigned
        druidWeapon = (MagicWeapon) WeaponFactory.getItem(WeaponType.Wand, new Rare());
        System.out.println(Color.CYAN+"Druid weapon:\n"+druidWeapon.getRarity().getItemRarityColor()+druidWeapon.getRarity().getRarityName()+" "+
                druidWeapon.getWeaponType()+ "| WeaponModifier: "+druidWeapon.getMagicPowerModifier() + " | WeaponRarityModifiwe: " +
                druidWeapon.getRarity().getPowerModifier());
    }
}
