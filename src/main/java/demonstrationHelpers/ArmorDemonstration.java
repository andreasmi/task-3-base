package main.java.demonstrationHelpers;

import main.java.consolehelpers.Color;
import main.java.factories.ArmorFactory;
import main.java.items.armor.Cloth;
import main.java.items.armor.Leather;
import main.java.items.armor.Plate;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.*;

import java.util.ArrayList;
import java.util.Random;

public class ArmorDemonstration {

    Armor currentArmor;

    public void armorDemonstration(){
        //Lists of armor
        ArrayList<Cloth> clothArmor = new ArrayList<>();//Collection of cloth armor
        ArrayList<Leather> leatherArmor = new ArrayList<>();//Collection of leather armor

        ArrayList<Armor> armorList = new ArrayList<>(); //Colleaction of random armor (Polymorphismn)

        //Cloth
        Armor commonCloth = ArmorFactory.getArmor(ArmorType.Cloth, new Common());
        Armor epicCloth = ArmorFactory.getArmor(ArmorType.Cloth, new Epic());
        Armor legendaryCloth = ArmorFactory.getArmor(ArmorType.Cloth, new Legendary());
        Armor rareCloth = ArmorFactory.getArmor(ArmorType.Cloth, new Rare());
        Armor uncommonCloth = ArmorFactory.getArmor(ArmorType.Cloth, new Uncommon());
        clothArmor.add((Cloth)commonCloth);
        clothArmor.add((Cloth)epicCloth);
        clothArmor.add((Cloth)legendaryCloth);
        clothArmor.add((Cloth)rareCloth);
        clothArmor.add((Cloth)uncommonCloth);

        //Leather
        Armor commonLeather = ArmorFactory.getArmor(ArmorType.Leather, new Common());
        Armor legendrayLeather = ArmorFactory.getArmor(ArmorType.Leather, new Legendary());
        leatherArmor.add((Leather)commonLeather);
        leatherArmor.add((Leather)legendrayLeather);

        //Array with all armor
        armorList.addAll(clothArmor);
        armorList.addAll(leatherArmor);

        //Adding other armor types to armor list
        armorList.add(ArmorFactory.getArmor(ArmorType.Plate, new Uncommon()));
        armorList.add(ArmorFactory.getArmor(ArmorType.Plate, new Epic()));
        armorList.add(ArmorFactory.getArmor(ArmorType.Mail, new Legendary()));
        armorList.add(ArmorFactory.getArmor(ArmorType.Mail, new Rare()));
        armorList.add(ArmorFactory.getArmor(ArmorType.Plate, new Common()));
        armorList.add(ArmorFactory.getArmor(ArmorType.Cloth, new Uncommon()));

        System.out.println(Color.CYAN+"Printing armor from cloth list"+Color.RESET);
        for(Armor armor : clothArmor){
            System.out.println(armor.getRarityColor()+"ArmorType: " + armor.getArmorType() + " | HealthModifier: " +  armor.getHealthModifier() + " | PhysicalModifier: " + armor.getPhysRedModifier() + Color.RESET);
        }

        System.out.println(Color.CYAN+"Printing armor from leather list"+Color.RESET);
        for(Armor armor : leatherArmor){
            System.out.println(armor.getRarityColor()+"ArmorType: " + armor.getArmorType() + " | HealthModifier: " +  armor.getHealthModifier() + " | PhysicalModifier: " + armor.getPhysRedModifier() + Color.RESET);
        }


        System.out.println(Color.CYAN+"Printing armor from armor list"+Color.RESET);
        for(Armor armor : armorList){
            System.out.println(armor.getRarityColor()+"ArmorType: " + armor.getArmorType() + " | HealthModifier: " +  armor.getHealthModifier() + " | PhysicalModifier: " + armor.getPhysRedModifier() + Color.RESET);
        }

        System.out.println("\n\n"+Color.CYAN+"Selecting 10 random armors from the armor list and change the currentArmor variable");
        for(int i = 0; i < 10; i++){
            currentArmor = armorList.get(new Random().nextInt(armorList.size()));
            System.out.println(currentArmor.getRarityColor()+"ArmorType: " + currentArmor.getArmorType() + " | HealthModifier: " +  currentArmor.getHealthModifier() + " | PhysicalModifier: " + currentArmor.getPhysRedModifier() + Color.RESET);
        }

    }
}
