package main.java.demonstrationHelpers;

import main.java.characters.Caster.Mage;
import main.java.characters.Melee.Paladin;
import main.java.characters.Support.Druid;
import main.java.characters.Support.Priest;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.abstractions.Healer;
import main.java.characters.abstractions.Shielder;
import main.java.factories.CharacterFactory;
import main.java.factories.SpellFactory;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.BladedWeapon;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.items.weapons.magic.Staff;
import main.java.items.weapons.melee.Axe;
import main.java.spells.abstractions.HealingSpell;
import main.java.spells.abstractions.ShieldingSpell;
import main.java.spells.abstractions.SpellType;
import main.java.spells.damaging.ArcaneMissile;

import static main.java.characters.abstractions.CharacterType.Priest;

public class CharacterDemonstartion {
    public void characterDemonstartion(){
        Character mage = new Mage();
        BladedWeapon notMagic = new Axe(new Common(), WeaponType.Axe);
        MagicWeapon magic = new Staff(new Common(), WeaponType.Staff);
        System.out.println("Mage weapon, not specified");
        System.out.println(mage.getWeapon());
        System.out.println("Mage weapon when equipped magic");
        mage.equipWeapon((Weapon)magic);
        System.out.println(mage.getWeapon());
        System.out.println("Mage weapon when equipped non magic");
        mage.equipWeapon((Weapon)notMagic);
        System.out.println(mage.getWeapon());

        Character priest = (Priest) CharacterFactory.getCharacter(Priest);
        priest.equipWeapon((Weapon)magic);
        ((Priest) priest).setShieldingSpell((ShieldingSpell) SpellFactory.getSpell(SpellType.Barrier));
        System.out.println("Priest shielding: " + ((Priest) priest).shieldPartyMember(2));

        Healer druid = (Healer) CharacterFactory.getCharacter(CharacterType.Druid);
        ((Druid) druid).equipWeapon((Weapon)magic);
        ((Druid) druid).setHealingSpell((HealingSpell) SpellFactory.getSpell(SpellType.Regrowth));
        System.out.println("Druid healing: " + druid.healPartyMember());

    }
}
