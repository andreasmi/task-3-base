package main.java.demonstrationHelpers;

import main.java.factories.SpellFactory;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.items.weapons.magic.Staff;
import main.java.items.weapons.melee.Axe;
import main.java.spells.abstractions.*;
import main.java.spells.damaging.ArcaneMissile;

public class SpellDemonstration {
    public void spellDemonstration(){
        DamagingSpell arcaneMissile = (DamagingSpell) SpellFactory.getSpell(SpellType.ArcaneMissile);
        DamagingSpell chaosBolt = (DamagingSpell) SpellFactory.getSpell(SpellType.ChaosBolt);
        HealingSpell regrowth = (HealingSpell) SpellFactory.getSpell(SpellType.Regrowth);
        ShieldingSpell barrier = (ShieldingSpell) SpellFactory.getSpell(SpellType.Barrier);

        System.out.println(arcaneMissile.getSpellName() + " " + arcaneMissile.getSpellDamageModifier());
        System.out.println(chaosBolt.getSpellName() + " " + chaosBolt.getSpellDamageModifier());
        System.out.println(regrowth.getSpellName() + " " + regrowth.getHealingAmount());
        System.out.println(barrier.getSpellName() + " " + barrier.getAbsorbShieldPercentage());


        Class allowedWeapon = MagicWeapon.class;
        Weapon notMagic = new Axe(new Common(), WeaponType.Axe);
        Weapon magic = new Staff(new Common(), WeaponType.Staff);
        if(allowedWeapon.isAssignableFrom(notMagic.getClass())){
            System.out.println("FALSE");
        }
        if(allowedWeapon.isAssignableFrom(magic.getClass())){
            System.out.println("TRUE");
        }

    }
}
