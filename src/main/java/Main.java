package main.java;

import main.java.consolehelpers.Color;
import main.java.demonstrationHelpers.*;

public class Main {
    public static void main(String[] args) {
        //Fields for demonstration classes
        ArmorDemonstration armorDemonstration = new ArmorDemonstration();
        WeaponDemonstration weaponDemonstration = new WeaponDemonstration();
        SpellDemonstration spellDemonstration = new SpellDemonstration();
        CharacterDemonstartion characterDemonstartion = new CharacterDemonstartion();
        GameDemonstration gameDemonstration = new GameDemonstration();

        //armorDemonstration.armorDemonstration();
        //weaponDemonstration.weaponDesmonstration();
        //spellDemonstration.spellDemonstration();
        //characterDemonstartion.characterDemonstartion();
        gameDemonstration.GameDemonstration();
    }
}
