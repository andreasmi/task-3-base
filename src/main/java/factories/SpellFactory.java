package main.java.factories;

import main.java.spells.abstractions.Spell;
import main.java.spells.abstractions.SpellType;
import main.java.spells.damaging.ArcaneMissile;
import main.java.spells.damaging.ChaosBolt;
import main.java.spells.healing.Regrowth;
import main.java.spells.healing.SwiftMend;
import main.java.spells.shielding.Barrier;
import main.java.spells.shielding.Rapture;

public class SpellFactory {
    /**
     * Gets a new spell
     * @param spellType The type of spell to create
     * @return A new instance of spell
     */
    public static Spell getSpell(SpellType spellType) {
        switch(spellType) {
            case Barrier:
                return new Barrier();
            case ChaosBolt:
                return new ChaosBolt();
            case Regrowth:
                return new Regrowth();
            case Rapture:
                return new Rapture();
            case Swiftmend:
                return new SwiftMend();
            case ArcaneMissile:
                return new ArcaneMissile();
            default:
                return null;
        }
    }
}
