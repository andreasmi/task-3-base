package main.java.factories;
// Imports
import main.java.characters.Caster.Mage;
import main.java.characters.Caster.Warlock;
import main.java.characters.Melee.Paladin;
import main.java.characters.Melee.Rogue;
import main.java.characters.Melee.Warrior;
import main.java.characters.Ranged.Ranger;
import main.java.characters.Support.Druid;
import main.java.characters.Support.Priest;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.CharacterType;

/**
 This factory exists to be responsible for creating new enemies.
*/
public class CharacterFactory {
    /**
     * Gets a new instance of a character
     * @param characterType The type of character to be created
     * @return The new character
     */
    public static Character getCharacter(CharacterType characterType) {
        switch(characterType) {
            case Mage:
                return new Mage();
            case Warlock:
                return new Warlock();
            case Paladin:
                return new Paladin();
            case Rogue:
                return new Rogue();
            case Warrior:
                return new Warrior();
            case Ranger:
                return new Ranger();
            case Druid:
                return new Druid();
            case Priest:
                return new Priest();
            default:
                return null;
        }
    }
}
