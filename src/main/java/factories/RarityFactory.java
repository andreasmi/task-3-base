package main.java.factories;

import main.java.items.rarity.*;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;


public class RarityFactory {
    /**
     * Gets a new rarity
     * @param itemRarity The rarity type to create
     * @return A new instance of rarity
     */
    public static Rarity getRarity(ItemRarity itemRarity) {
        switch(itemRarity) {
            case Common:
                return new Common();
            case Epic:
                return new Epic();
            case Legendary:
                return new Legendary();
            case Rare:
                return new Rare();
            case Uncommon:
                return new Uncommon();
            default:
                return null;
        }
    }
}
