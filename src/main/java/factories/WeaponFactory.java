package main.java.factories;
// Imports
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.items.weapons.magic.Staff;
import main.java.items.weapons.magic.Wand;
import main.java.items.weapons.melee.*;
import main.java.items.weapons.ranged.Bow;
import main.java.items.weapons.ranged.Crossbow;
import main.java.items.weapons.ranged.Gun;
/**
 This factory exists to be responsible for creating new enemies.
*/
public class WeaponFactory {
    /**
     * Gets a new weapon
     * @param weaponType The type of weapon to create
     * @param rarity The rarity of the weapon to create
     * @return A new instance of a weapon
     */
    public static Weapon getItem(WeaponType weaponType, Rarity rarity) {
        switch(weaponType) {
            case Axe:
                return new Axe(rarity, weaponType);
            case Bow:
                return new Bow(rarity, weaponType);
            case Crossbow:
                return new Crossbow(rarity, weaponType);
            case Dagger:
                return new Dagger(rarity, weaponType);
            case Gun:
                return new Gun(rarity, weaponType);
            case Hammer:
                return new Hammer(rarity, weaponType);
            case Mace:
                return new Mace(rarity, weaponType);
            case Staff:
                return new Staff(rarity, weaponType);
            case Sword:
                return new Sword(rarity, weaponType);
            case Wand:
                return new Wand(rarity, weaponType);
            default:
                return null;
        }
    }
}
