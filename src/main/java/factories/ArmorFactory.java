package main.java.factories;
// Imports
import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.Cloth;
import main.java.items.armor.Leather;
import main.java.items.armor.Mail;
import main.java.items.armor.Plate;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.abstractions.Rarity;

/**
 This factory exists to be responsible for creating new Armor.
*/
public class ArmorFactory {
    /**
     * Gets a new armor
     * @param armorType The type of armor that is being made
     * @param rarity The rarity of the armor
     * @return A new instance of armor
     */
    public static Armor getArmor(ArmorType armorType, Rarity rarity){
        switch(armorType) {
            case Cloth:
                return new Cloth(ArmorStatsModifiers.CLOTH_HEALTH_MODIFIER, ArmorStatsModifiers.CLOTH_PHYS_RED_MODIFIER, ArmorStatsModifiers.CLOTH_MAGIC_RED_MODIFIER, rarity, ArmorType.Cloth);
            case Leather:
                return new Leather(ArmorStatsModifiers.LEATHER_HEALTH_MODIFIER, ArmorStatsModifiers.LEATHER_PHYS_RED_MODIFIER, ArmorStatsModifiers.LEATHER_MAGIC_RED_MODIFIER, rarity, ArmorType.Leather);
            case Mail:
                return new Mail(ArmorStatsModifiers.MAIL_HEALTH_MODIFIER, ArmorStatsModifiers.MAIL_PHYS_RED_MODIFIER, ArmorStatsModifiers.MAIL_MAGIC_RED_MODIFIER, rarity, ArmorType.Mail);
            case Plate:
                return new Plate(ArmorStatsModifiers.PLATE_HEALTH_MODIFIER, ArmorStatsModifiers.PLATE_PHYS_RED_MODIFIER, ArmorStatsModifiers.PLATE_MAGIC_RED_MODIFIER, rarity, ArmorType.Plate);
            default:
                return null;
        }
    }
}
