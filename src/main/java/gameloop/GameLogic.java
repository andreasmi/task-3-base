package main.java.gameloop;

public class GameLogic {
    /*
     The game logic goes as follows:
     1. Present main menu where the user can start a new game or see high scores (max floor reached and what party).
        e.g. Nick; Floor 50; Druid, Paladin, Rogue, Warrior.
     2. When the user selects new game, they are asked to create a party of 4 characters.
        They pick the 4 classes, the characters are generated and equipped with common items.
     3. After party creation they are presented with 3 options:
        - Easy encounter (normally scaled mobs)
        - Hard encounter (mobs scaled to be more difficult)
        - Town (A place to rest)
        3.1. If they pick town, the parties health is restored.
            TODO Implement gold cost for resting, and a shop to buy items.
     4. After an encounter type is picked, the encounter is generated.
        4.1. A group of enemies are randomly generated from the pool.
        4.2. These enemies are scaled based on the chosen difficulty for this floor and what floor they are on.
     5. The party then battles with the enemies generated for the floor.
        5.1. If the party is killed they user is taken to a game over screen.
            5.1.1. If they are in the top 10 runs, their name is asked and they are added to the leaderboard.
                    The leaderboard only contains the top 10 runs in order, so it needs to be updated.
     6. After the battle is over and the party has won, loot can drop.
        6.1. Loot is generated, better loot is given for harder difficulties and floor levels.
            TODO Implement a gold value of the items to sell them, this can be used to buy what the user wants in town.
     7. The floor number is incremented, and the user is presented with the options in step 3.
     8. The main game loop is steps 3-6.
    */
}
