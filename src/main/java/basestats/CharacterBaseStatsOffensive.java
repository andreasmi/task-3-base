package main.java.basestats;

/**
 * This class serves as a single place to balance all the offensive stats of the characters.
 */
public class CharacterBaseStatsOffensive {
    // Mage
    public static final double MAGE_MAGIC_POWER = 140;
    // Paladin
    public static final double PALADIN_MELEE_ATTACK_POWER = 100;
    // Ranger
    public static final double RANGER_RANGED_ATTACK_POWER = 140;
    // Rogue
    public static final double ROGUE_MELEE_ATTACK_POWER = 150;
    // Warlock
    public static final double WARLOCK_MAGIC_POWER = 120;
    // Warrior
    public static final double WARRIOR_MELEE_ATTACK_POWER = 160;
}
