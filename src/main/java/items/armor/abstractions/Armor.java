package main.java.items.armor.abstractions;

import main.java.items.rarity.abstractions.Rarity;

/**
 * Overridden methods from armor interface and fields for a armor.
 * Every armor will extend this class
 */
public abstract class Armor implements ArmorInterface{

    // Stat modifiers
    private final double healthModifier;
    private final double physRedModifier;
    private final double magicRedModifier;
    // Rarity
    private final Rarity rarity;
    //Armor type
    private final ArmorType armorType;

    public Armor(double healthModifier, double physRedModifier, double magicRedModifier, Rarity rarity, ArmorType armorType) {
        this.healthModifier = healthModifier;
        this.physRedModifier = physRedModifier;
        this.magicRedModifier = magicRedModifier;
        this.rarity = rarity;
        this.armorType = armorType;
    }

    /**
     * @return The health modifier
     */
    @Override
    public double getHealthModifier() {
        return healthModifier;
    }

    /**
     * @return The Physical reduction modifier
     */
    @Override
    public double getPhysRedModifier() {
        return physRedModifier;
    }

    /**
     * @return The magical reduction modifier
     */
    @Override
    public double getMagicRedModifier() {
        return magicRedModifier;
    }

    /**
     * @return The rarity modifier
     */
    @Override
    public double getRarityModifier() {
        return rarity.getPowerModifier();
    }

    /**
     * @return The rarity color
     */
    @Override
    public String getRarityColor() {
        return rarity.getItemRarityColor();
    }

    /**
     * @return The armor type
     */
    @Override
    public ArmorType getArmorType() {
        return armorType;
    }
}
