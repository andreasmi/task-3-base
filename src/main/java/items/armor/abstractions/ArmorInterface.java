package main.java.items.armor.abstractions;

/**
 * Shared method for all armor classes
 */
public interface ArmorInterface {
    //Public properties that is sheared by all armor
    double getHealthModifier();
    double getPhysRedModifier();
    double getMagicRedModifier();
    double getRarityModifier();
    String getRarityColor();
    ArmorType getArmorType();
}