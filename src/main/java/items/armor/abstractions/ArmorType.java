package main.java.items.armor.abstractions;

/**
 * A list of the different existing armors
 */
public enum ArmorType {
    Cloth,
    Leather,
    Mail,
    Plate
}
