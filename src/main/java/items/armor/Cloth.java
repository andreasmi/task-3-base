package main.java.items.armor;
// Imports
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.abstractions.Rarity;

public class Cloth extends Armor {
    /**
     * @param healthModifier The health modifier of the armor
     * @param physRedModifier The physical reduction modifier of the armor
     * @param magicRedModifier The magical reduction modifier of the armor
     * @param rarity The rarity of the armor
     * @param armorType The armor type
     */
    public Cloth(double healthModifier, double physRedModifier, double magicRedModifier, Rarity rarity, ArmorType armorType) {
        super(healthModifier, physRedModifier, magicRedModifier, rarity, armorType);
    }
}
