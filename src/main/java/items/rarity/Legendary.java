package main.java.items.rarity;

import main.java.basestats.ItemRarityModifiers;
import main.java.consolehelpers.Color;
import main.java.items.rarity.abstractions.Rarity;

public class Legendary implements Rarity {
    /**
     * @return The power modifier of the rarity
     */
    @Override
    public double getPowerModifier() {
        return ItemRarityModifiers.LEGENDARY_RARITY_MODIFIER;
    }
    /**
     * @return The color of the rarity
     */
    @Override
    public String getItemRarityColor() {
        return Color.YELLOW;
    }
    /**
     * @return The name of the rarity
     */
    @Override
    public String getRarityName(){return "Legendary";}
}
