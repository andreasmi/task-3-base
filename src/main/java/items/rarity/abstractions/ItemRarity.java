package main.java.items.rarity.abstractions;

/**
 * The different rarities that exists
 */
public enum ItemRarity {
    Common,
    Uncommon,
    Rare,
    Epic,
    Legendary
}
