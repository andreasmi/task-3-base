package main.java.items.rarity.abstractions;

/**
 * Every raraity needs to implement these functions to work properly
 */
public interface Rarity {
    double getPowerModifier();
    String getItemRarityColor();
    String getRarityName();
}
