package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Common;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.BluntWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;

public class Hammer extends Weapon implements BluntWeapon {
    public Hammer(Rarity rarity, WeaponType weaponType) {
        super(rarity, weaponType);
    }

    public Hammer(WeaponType weaponType) {
        super(new Common(), weaponType);
    }

    /**
     * Gets the power modifier of the wepaon
     * @return The Attack power modifier of the weapon
     */
    @Override
    public double getAttackPowerModifier() {
        return WeaponStatsModifiers.HAMMER_ATTACK_MOD;
    }

}
