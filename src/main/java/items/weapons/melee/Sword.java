package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Common;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.BladedWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;

public class Sword extends Weapon implements BladedWeapon {
    public Sword(Rarity rarity, WeaponType weaponType) {
        super(rarity, weaponType);
    }

    public Sword(WeaponType weaponType) {
        super(new Common(), weaponType);
    }

    /**
     * Gets the power modifier of the wepaon
     * @return The Attack power modifier of the weapon
     */
    @Override
    public double getAttackPowerModifier() {
        return WeaponStatsModifiers.SWORD_ATTACK_MOD;
    }

}
