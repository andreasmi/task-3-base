package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Common;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.RangedWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;

public class Bow extends Weapon implements RangedWeapon {

    public Bow(Rarity rarity, WeaponType weaponType) {
        super(rarity, weaponType);
    }

    public Bow(WeaponType weaponType) {
        super(new Common(), weaponType);
    }

    /**
     * Gets the power modifier of the wepaon
     * @return The Attack power modifier of the weapon
     */
    @Override
    public double getAttackPowerModifier() {
        return WeaponStatsModifiers.BOW_ATTACK_MOD;
    }

}
