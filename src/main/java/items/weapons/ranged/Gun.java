package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Common;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.RangedWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;

public class Gun extends Weapon implements RangedWeapon {
    public Gun(Rarity rarity, WeaponType weaponType) {
        super(rarity, weaponType);
    }

    /**
     * Creating a new Gun instance
     * @param weaponType The type of weapon this is
     */
    public Gun(WeaponType weaponType) {
        super(new Common(), weaponType);
    }

    /**
     * Gets the power of the current weapon
     * @return double The power of the current weapon
     */
    @Override
    public double getAttackPowerModifier() {
        return WeaponStatsModifiers.GUN_ATTACK_MOD;
    }

}
