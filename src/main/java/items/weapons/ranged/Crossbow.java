package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Common;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.RangedWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;

public class Crossbow extends Weapon implements RangedWeapon {
    public Crossbow(Rarity rarity, WeaponType weaponType) {
        super(rarity, weaponType);
    }

    public Crossbow(WeaponType weaponType) {
        super(new Common(), weaponType);
    }

    /**
     * Gets the power modifier of the wepaon
     * @return The Attack power modifier of the weapon
     */
    @Override
    public double getAttackPowerModifier() {
        return WeaponStatsModifiers.CROSSBOW_ATTACK_MOD;
    }

}
