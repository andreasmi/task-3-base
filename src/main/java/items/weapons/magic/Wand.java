package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Common;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;

public class Wand extends Weapon implements MagicWeapon {
    // Stat modifiers
    private double magicPowerModifier = WeaponStatsModifiers.WAND_MAGIC_MOD;

    public Wand(Rarity rarity, WeaponType weaponType) {
        super(rarity, weaponType);
    }

    public Wand(WeaponType weaponType){
        super(new Common(), weaponType);
    }

    /**
     * Gets the power modifier of the wepaon
     * @return The Magic power modifier of the weapon
     */
    @Override
    public double getMagicPowerModifier() {
        return WeaponStatsModifiers.WAND_MAGIC_MOD;
    }

}
