package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Common;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;

public class Staff extends Weapon implements MagicWeapon {

    public Staff(Rarity rarity, WeaponType weaponType) {
        super(rarity, weaponType);
    }
    public Staff(WeaponType weaponType){
        super(new Common(), weaponType);
    }

    /**
     * Gets the power modifier of the wepaon
     * @return The Magic power modifier of the weapon
     */
    @Override
    public double getMagicPowerModifier() {
        return WeaponStatsModifiers.STAFF_MAGIC_MOD;
    }


}
