package main.java.items.weapons.abstractions;

/**
 * Interface for BladedWeapon
 */
public interface BladedWeapon extends iWeapon {
    double getAttackPowerModifier();
}
