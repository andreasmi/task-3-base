package main.java.items.weapons.abstractions;

/**
 * The different types of weapons that exists
 */
public enum WeaponType {
    Axe,
    Bow,
    Crossbow,
    Dagger,
    Gun,
    Hammer,
    Mace,
    Staff,
    Sword,
    Wand
}
