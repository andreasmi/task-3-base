package main.java.items.weapons.abstractions;

import main.java.items.rarity.abstractions.Rarity;
/**
 * Every weapon has it's own function to retrieve attack/magic power modifier, that is based on which interface they implements
 * Every weapon implements one kind of weapon interface (xWeapon interface), these will be used to make sure that a character isn't weilding a weapon that it's not allowed to.
 *
 * This is an abstract weapon class which includes functionality for the iWeapon interface.
 * Every weapon class needs to extends this class to be able to function properly.
*/

/*
*/

public abstract class Weapon {
    private final Rarity rarity;
    private final WeaponType weaponType;

    public Weapon(Rarity rarity, WeaponType weaponType){
        this.rarity = rarity;
        this.weaponType = weaponType;
    }

    /**
     * @return The rarity object that the weapon contains
     */
    public Rarity getRarity(){ return rarity; }

    /**
     * @return The wepaon type of this weapon
     */
    public WeaponType getWeaponType(){return weaponType; }

}
