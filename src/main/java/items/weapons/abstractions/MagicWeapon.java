package main.java.items.weapons.abstractions;

/**
 * Interface for MagicWeapon
 */
public interface MagicWeapon extends iWeapon {
    double getMagicPowerModifier();
}
