package main.java.items.weapons.abstractions;

/**
 * Interface for RangedWeapon
 */
public interface RangedWeapon extends iWeapon {
    double getAttackPowerModifier();
}
