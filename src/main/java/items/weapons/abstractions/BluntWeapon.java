package main.java.items.weapons.abstractions;

/**
 * Interface for BluntWeapon
 */
public interface BluntWeapon extends iWeapon {
    double getAttackPowerModifier();
}
