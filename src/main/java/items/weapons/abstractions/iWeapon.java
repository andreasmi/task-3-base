package main.java.items.weapons.abstractions;

import main.java.items.rarity.abstractions.Rarity;

/**
 * Interface with methods that every weapon should have
 */
public interface iWeapon {
    Rarity getRarity();
    WeaponType getWeaponType();
}
