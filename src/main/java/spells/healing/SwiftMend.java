package main.java.spells.healing;

import main.java.basestats.SpellModifiers;
import main.java.spells.abstractions.HealingSpell;

public class SwiftMend implements HealingSpell {

    @Override
    public double getHealingAmount() {
        return SpellModifiers.SWIFTMEND_HEAL;
    }

    @Override
    public String getSpellName() {
        return "Swiftmend";
    }
}
