package main.java.spells.damaging;

import main.java.basestats.SpellModifiers;
import main.java.spells.abstractions.DamagingSpell;

public class ChaosBolt implements DamagingSpell {

    @Override
    public double getSpellDamageModifier() {
        return SpellModifiers.CHAOS_BOLT_DAMAGE_MODIFIER;
    }

    @Override
    public String getSpellName() {
        return "Chaos Bolt";
    }
}
